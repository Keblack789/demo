// Get the packages we need
var express = require('express');
var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;
var bodyParser = require('body-parser');
var morgan = require('morgan')
// var cors = require('cors')

// Models
var Test = require('./models/test');

// Controllers
var TestController = require('./controllers/test');
var NodeController = require('./controllers/node');
var ExploitController = require('./controllers/exploit');

// Connect to MongoDB
const DB_NAME = 'main_server';
mongoose.connect('mongodb://localhost:27017/' + DB_NAME, { useMongoClient: true });

// Create our Express application
var app = express();

var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.send(200);
  }
  else {
    next();
  }
};

// Add middleware
app.use(allowCrossDomain);
app.use(bodyParser.json());
app.use(morgan('tiny', { immediate: true }))
// app.use(cors());

// Use environment defined port or 3000
var port = process.env.PORT || 9000;

// Create our Express router
var router = express.Router();

// Initial dummy route for testing
router.get('/', function(req, res) {
    res.send(200);
});

// Routes for /tests
router.route('/tests')
        .post(TestController.postTests)
        .get(TestController.getTests);

router.route('/tests/:testId')
        .get(TestController.getTest)
        .put(TestController.putTest)
        .delete(TestController.delTest);

router.route('/tests/:testId/update')
        .post(TestController.postUpdateTest);

router.route('/tests/:testId/start')
        .post(TestController.postStartTest);

router.route('/tests/:testId/stop')
        .post(TestController.postStopTest);

// Routes for /nodes
router.route('/nodes')
        .post(NodeController.postNodes)
        .get(NodeController.getNodes);

router.route('/nodes/:nodeId')
        .get(NodeController.getNode)
        .put(NodeController.putNode)
        .delete(NodeController.delNode);

// Routes for /exploits
router.route('/exploits')
        .post(ExploitController.postExploits)
        .get(ExploitController.getExploits);

router.route('/exploits/:exploitId')
        .get(ExploitController.getExploit)
        .put(ExploitController.putExploit)
        .delete(ExploitController.delExploit);

// Register all our routes with /api
app.use('/api', router);

// Start the server
app.listen(port, function(){
    console.log("Listening on: " + port);
});