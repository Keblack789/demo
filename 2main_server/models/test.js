// Load required packages
var mongoose = require('mongoose');

// Define our test schema
var TestSchema   = new mongoose.Schema({
  running: Boolean,
  name: String,
  exploit_id: String,
  attacker_ip: String,
  defender_ip: String,
  updates: {
    cpu_values: [Number],
    disk_values: [Number],
    ram_values: [Number],
    timestamp_values: [Number]
  }
});

// Export the Mongoose model
module.exports = mongoose.model('Test', TestSchema);