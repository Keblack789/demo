// Load required packages
var mongoose = require('mongoose');

// Define our node schema
var NodeSchema   = new mongoose.Schema({
  name: String,
  vm: Boolean,
  ip: String,
  os: String,
  patch: String,
  type: String
});

// Export the Mongoose model
module.exports = mongoose.model('Node', NodeSchema);