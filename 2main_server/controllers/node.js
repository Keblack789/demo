// Dependencies
var Validator = require('jsonschema').Validator;
var mongoose = require('mongoose');
mongoose.Promise = require('q').Promise;

// Models
var Node = require('../models/node');

// Create endpoint /api/nodes for POST
exports.postNodes = function(req, res){
  // Create a new JSON validator
  var v = new Validator();  

  // Create JSON Schema for POST /api/nodes
  var nodeSchema = {
    id: '/NewNode',
    type: 'object',
    properties: {
      name: { type: 'string' },
      vm: { type: 'boolean' },
      ip: { type: 'string' },
      os: { type: 'string', enum: [ "Windows 10", "Windows 8", "Windows 7", "Windows XP", "MacOSX", "Ubuntu16.04" ] },
      patch: { type: 'string' },
      type: { type: 'string', enum: [ "attacker", "defender" ] }
    },
    required: ['name', 'vm', 'ip', 'os', 'patch', 'type']
  };

  // Check if JSON data is valid
  var results = v.validate(req.body, nodeSchema);

  // If not valid send 400 with err msg
  if (!results.valid){
    var err_msg = ""
    if (results.errors.length > 0){
      err_msg = results.errors[0].property + " " + results.errors[0].message;
    }
    return res.status(400).send({ msg: err_msg });
  }
  
  // Create a new instance of the node model
  var node = new Node();

  // Set the node properties that came from the POST data
  node.name = req.body.name;
  node.vm = req.body.vm;
  node.ip = req.body.ip;
  node.os = req.body.os;
  node.patch = req.body.patch;
  node.type = req.body.type;

  node.save(function(err){
    if (err){
      return res.status(500).send(err);
    }
    return res.status(200).send({ msg: "Node successfully added" });
  });
};

// Create endpoint /api/nodes for GET
exports.getNodes = function(req, res){
  // Use the Node model to find all nodes
  Node.find(function(err, nodes){
    if (err){
      return res.status(500).send(err);
    }
    return res.status(200).send(nodes);
  });
};

// Create endpoint /api/nodes/:nodeId for GET
exports.getNode = function(req, res){
  var nodeId = req.params.nodeId;

  if (!mongoose.Types.ObjectId.isValid(nodeId)){
    return res.status(400).send({ msg: nodeId + " is not a valid id" });
  }

  // Use the Node model to find a specific node
  Node.findById(nodeId, function(err, node){
    if (err){
      res.status(500).send(err);
    }
    if (!node){
      return res.status(404).send({ msg: "No node with id " + nodeId })
    }
    return res.status(200).send(node);
  });
};

// Create endpoint /api/nodes/:nodeId for PUT
exports.putNode = function(req, res){
  var nodeId = req.params.nodeId;

  if (!mongoose.Types.ObjectId.isValid(nodeId)){
    return res.status(400).send({ msg: nodeId + " is not a valid id" });
  }

  // Use the Node model to find a specific node
  Node.findById(nodeId, function(err, node){
    if (err){
      res.status(500).send(err);
    }
    if (!node){
      return res.status(404).send({ msg: "No node with id " + nodeId })
    }

    // Create a new JSON validator
    var v = new Validator();  

    // Create JSON Schema for POST /api/nodes
    var nodeSchema = {
      id: '/UpdateNode',
      type: 'object',
      properties: {
        name: { type: 'string' },
        vm: { type: 'boolean' },
        ip: { type: 'string' },
        os: { type: 'string', enum: [ "Windows 10", "Windows 8", "Windows 7", "Windows XP", "MacOSX", "Ubuntu16.04" ] },
        patch: { type: 'string' },
        type: { type: 'string', enum: [ "attacker", "defender" ] }
      }
    };

    // Check if JSON data is valid
    var results = v.validate(req.body, nodeSchema);

    // If not valid send 400 with err msg
    if (!results.valid){
      var err_msg = ""
      if (results.errors.length > 0){
        err_msg = results.errors[0].property + " " + results.errors[0].message;
      }
      return res.status(400).send({ msg: err_msg });
    }
      
    Object.keys(nodeSchema['properties']).forEach(function(key){
      if (key in req.body){
        node[key] = req.body[key];
      }
    }, this);
    
    node.save(function(err){
      if (err){
        return res.status(500).send(err);
      }
      return res.status(200).send({ msg: "Updated node successfully", node: node });
    });
  });
};

// Create endpoint /api/nodes/:nodeId for DELETE
exports.delNode = function(req, res){
  var nodeId = req.params.nodeId;

  if (!mongoose.Types.ObjectId.isValid(nodeId)){
    return res.status(400).send({ msg: nodeId + " is not a valid id" });
  }

  // Use the Node model to find a specific node and remove it
  Node.findByIdAndRemove(nodeId, function(err, node){
    if (err){
      return res.status(500).send(err);
    }
    if (!node){
      return res.status(404).send({ msg: "No node with id: " + nodeId });
    }
    return res.status(200).send({ msg: "Node deleted successfully" });
  });

};