// Dependencies
var Validator = require('jsonschema').Validator;
var mongoose = require('mongoose');
var Q = require('q');
mongoose.Promise = Q.Promise;
var querystring = require('query-string');
var http = require('http');

// Configuration
var REQUEST_TIMEOUT = 5000;

// Models
var Test = require('../models/test');

// Create endpoint /api/tests for POST
exports.postTests = function(req, res){
  // Create a new JSON validator
  var v = new Validator();  

  // Create JSON Schema for POST /api/tests
  var testSchema = {
    id: '/NewTest',
    type: 'object',
    properties: {
      name: { type: 'string' },
      exploit_id: { type: 'string' },
      attacker_ip: { type: 'string' },
      defender_ip: { type: 'string' }
    },
    required: ['name', 'exploit_id', 'attacker_ip', 'defender_ip']
  };

  // Check if JSON data is valid
  var results = v.validate(req.body, testSchema);

  // If not valid send 400 with err msg
  if (!results.valid){
    var err_msg = ""
    if (results.errors.length > 0){
      err_msg = results.errors[0].property + " " + results.errors[0].message;
    }
    return res.status(400).send({ msg: err_msg });
  }

  // Create a new instance of the test model
  var test = new Test();

  // Set the test properties that came from the POST data
  test.name = req.body.name;
  test.exploit_id = req.body.exploit_id;
  test.attacker_ip = req.body.attacker_ip;
  test.defender_ip = req.body.defender_ip;

  // Set other properties
  test.running = false;
  test.completed = false;

  // Save test and check for erros
  test.save(function(err){
    if (err){
      return res.status(500).send(err);
    }
    return res.status(200).send({ msg: "Test successfully added" });
  });
};

// Create endpoint /api/tests for GET
exports.getTests = function(req, res){
  // Use the Test model to find all tests
  Test.find(function(err, tests){
    if (err){
      return res.status(500).send(err);
    }
    return res.status(200).send(tests);
  });

};

// Create endpoint /api/tests/:testId for GET
exports.getTest = function(req, res){
  var testId = req.params.testId;

  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Use the Test model to find a specific node
  Test.findById(testId, function(err, test){
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId })
    }
    return res.status(200).send(test);
  })
};

// Create endpoint /api/tests/:testId for PUT
exports.putTest = function(req, res){
  var testId = req.params.testId;

  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Use the Test model to find a specific test
  Test.findById(testId, function(err, test) {
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId });
    }
    // Create a new JSON validator
    var v = new Validator();  
  
    // Create JSON Schema for POST /api/tests/:testId
    var testSchema = {
      id: '/UpdateTest',
      type: 'object',
      properties: {
        name: { type: 'string' }
      }
    };
  
    // Check if JSON data is valid
    var results = v.validate(req.body, testSchema);
  
    // If not valid send 400 with err msg
    if (!results.valid){
      var err_msg = ""
      if (results.errors.length > 0){
        err_msg = results.errors[0].property + " " + results.errors[0].message;
      }
      return res.status(400).send({ msg: err_msg });
    }

    Object.keys(testSchema['properties']).forEach(function(key) {
      if (key in req.body){
        test[key] = req.body[key];
      }
    }, this);

    // Save the test and check for errors
    test.save(function(err) {
      if (err){
        return res.status(500).send(err);
      }

      return res.send({ msg: "Updated test successfully", test: test });
    });
  });

  
};

// Create endpoint /api/tests/:testId for DELETE
exports.delTest = function(req, res){
  var testId = req.params.testId;

  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Use the Test model to find a specific test and remove it
  Test.findByIdAndRemove(req.params.testId, function(err, test) {
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId });
    }
    return res.status(200).send({ msg: 'Test deleted successfully' });
  });
}

// Create endpoint /api/tests/:testId/update for POST
exports.postUpdateTest = function(req, res){
  // Validate testId
  var testId = req.params.testId;
  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Create a new JSON validator
  var v = new Validator();  

  // Create JSON Schema for POST /api/tests/:testId/update
  var updateSchema = {
    id: '/TestReceiveUpdate',
    type: 'object',
    properties: {
      cpu: { type: 'number' },
      disk: { type: 'number' },
      ram: { type: 'number' },
      timestamp: { type: 'number' }
    },
    required: [ 'cpu', 'disk', 'ram', 'timestamp' ]
  };

  // Check if JSON data is valid
  var results = v.validate(req.body, updateSchema);

  // If not valid send 400 with err msg
  if (!results.valid){
    var err_msg = ""
    if (results.errors.length > 0){
      err_msg = results.errors[0].property + " " + results.errors[0].message;
    }
    return res.status(400).send({ msg: err_msg });
  }


  // Use the Test model to find a specific test
  Test.findById(testId, function(err, test) {
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId });
    }

    // Update the updates property of test
    if ( !('updates' in test) ){
      test.updates = {
        cpu_values: [req.body.cpu],
        disk_values: [req.body.disk],
        ram_values: [req.body.ram],
        timestamp_values: [req.body.timestamp]
      };
    } else {
      test.updates.cpu_values.push(req.body.cpu);
      test.updates.disk_values.push(req.body.disk);
      test.updates.ram_values.push(req.body.ram);
      test.updates.timestamp_values.push(req.body.timestamp);
    }

    // Save the test and check for errors
    test.save(function(err) {
      if (err){
        return res.status(500).send(err);
      }

      return res.send({ msg: "Updated test successfully", test: test });
    });
  });
};

// Create endpoint /api/tests/:testId/start for POST
exports.postStartTest = function(req, res){
  // Validate testId
  var testId = req.params.testId;
  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Use the Test model to find a specific test
  Test.findById(testId, function(err, test) {
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId });
    }

    var passed_data = {
      test_id: test._id,      
      testname: test.name,
      exploit_id: test.exploit_id,
      attacker_ip: test.attacker_ip,
      defender_ip: test.defender_ip
    }

    // TODO Start the test
    sendDefendRequestToDefender(passed_data)
    .then(
    function(passed_data){
      sendAttackRequest(passed_data)
      .then(
        function(passed_data){
          // Set running to true
          test.running = true;
    
          // Save the test and check for errors
          test.save(function(err) {
            if (err){
              return res.status(500).send(err);
            }
    
            return res.send({ msg: "Started test successfully", test: test });
          });
        }, 
        function(err){
          return res.status(500).send({ msg: "Could not contact attacker: " + test.attacker_ip });
        });
    },
    function(err){
      console.log(err);
      return res.status(500).send({ msg: "Could not contact defender: " + test.defender_ip });
    });


  
  });

};

// Create endpoint /api/tests/:testId/stop for POST
exports.postStopTest = function(req, res){
  // Validate testId
  var testId = req.params.testId;
  if (!mongoose.Types.ObjectId.isValid(testId)){
    return res.status(400).send({ msg: testId + " is not a valid id" });
  }

  // Use the Test model to find a specific test
  Test.findById(testId, function(err, test) {
    if (err){
      return res.status(500).send(err);
    }
    if (!test){
      return res.status(404).send({ msg: "No test with id: " + testId });
    }

    // TODO Stop the test

    // Set running to false
    test.running = false;

    // Save the test and check for errors
    test.save(function(err) {
      if (err){
        return res.status(500).send(err);
      }

      return res.send({ msg: "Stopped test successfully", test: test });
    });
  });
};

function sendDefendRequestToDefender(passed_data){
  var deferred = Q.defer()

  var defender_ip = passed_data.defender_ip;

  // POST JSON Object to send to Defender
  var defenderPostData = {
      "testname": passed_data.testname,
      "exploit_id": passed_data.exploit_id,
      "attacker_ip": passed_data.attacker_ip,
      "defender_ip": passed_data.defender_ip,
      "test_id": passed_data.test_id
  }

  defenderPostData = querystring.stringify(defenderPostData);
  console.log("Sending Defend Request");
  var options = {
      hostname: defender_ip,
      port: 9000,
      path: '/start_defend',
      method: 'POST', 
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(defenderPostData)
      },
      timeout: REQUEST_TIMEOUT
  }

  req = http.request(options, (res) => {
      if(res.statusCode == 200){
          deferred.resolve(passed_data);
      } else {
          console.log("sendDefendRequest not 200");
          deferred.reject({"statusCode": 400, msg: "sendDefend request failed"});
      }
  });
  
  req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`);
      deferred.reject({"statusCode": 500, msg: `problem with request: ${e.message}` });
  });
  
  // write data to request body
  req.write(defenderPostData);
  req.end();

  return deferred.promise
}

function sendAttackRequest(passed_data){
  var deferred = Q.defer()

  var attacker_ip = passed_data.attacker_ip;
  
  // POST JSON Object to send to Attacker
  var attackerPostData = {
      "testname": passed_data.testname,
      "exploit_id": passed_data.exploit_id,
      "attacker_ip": passed_data.attacker_ip,
      "defender_ip": passed_data.defender_ip,
      "test_id": passed_data.test_id
  }

  attackerPostData = querystring.stringify(attackerPostData);
  console.log("Sending Attack Request");
  var options = {
      hostname: attacker_ip,
      port: 9000,
      path: '/start_attack',
      method: 'POST', 
      headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(attackerPostData)
      },
      timeout: REQUEST_TIMEOUT
  }

  req = http.request(options, (res) => {
      if(res.statusCode == 200){
          passed_data.statusCode = 200;
          passed_data.msg = "sendAttack request sent successfully";
          deferred.resolve(passed_data);
      } else {
          console.log("sendAttackRequest not 200: " + res.statusCode);
          deferred.reject({"statusCode": 400, msg: "sendAttack request failed"});
      }
  });
  
  req.on('error', (e) => {
      console.error(`problem with request: ${e.message}`);
      deferred.reject({ "statusCode": 500, msg: `problem with request: ${e.message}` });
  });
  
  // write data to request body
  req.write(attackerPostData);
  req.end();

  return deferred.promise
};
