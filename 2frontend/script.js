// create the module and name it listApp
var listApp = angular.module('listApp', ['ngRoute']);

listApp.config(function($routeProvider){
  $routeProvider
    // .when('/', {
    //   templateUrl: 'pages/main.html',
    //   controller: 'mainController'
    // })
    .when('/tests/:testId', {
      templateUrl: 'pages/test.html',
      controller: 'testController'
    })
    .when('/tests', {
      templateUrl: 'pages/tests.html',
      controller: 'testsController'
    })
    .when('/nodes/:nodeId', {
      templateUrl: 'pages/node.html',
      controller: 'nodeController'
    })
    .when('/nodes', {
      templateUrl: 'pages/nodes.html',
      controller: 'nodesController'
    })
    .when('/exploits/:exploitId', {
      templateUrl: 'pages/exploit.html',
      controller: 'exploitController'
    })
    .when('/exploits', {
      templateUrl: 'pages/exploits.html',
      controller: 'exploitsController'
    })
    .when('/newTest', {
      templateUrl: 'pages/newTest.html',
      controller: 'newTestController'
    })
    .otherwise({ redirectTo: '/tests'});
});

listApp.controller('mainController', function($scope){
});

listApp.controller('testController', function($scope, $http, $route, $routeParams, $window){
  // create a message to display in our view
  console.log('testController');
  var testId = $routeParams.testId;
  console.log(testId);
  $http({
    method: "GET",
    url: "http://localhost:9000/api/tests/" + testId 
  }).then(function(response) {
    console.log(response.data);
    $scope.test = response.data;
  }).catch(function(response){
    console.log("error with http get");
    console.log(response.data);
    $window.location.href = '/tests';
  });
});

// create the controller and inject Angular's $scope
listApp.controller('testsController', function($scope, $http, $window) {
  // console.log('testsController');
  // create a message to display in our view
  $scope.load = function(test){
    $window.location.href = '#!/tests/' + test._id;
  };
  $http({
    method: "GET",
    url: "http://localhost:9000/api/tests"
  }).then(function(response) {
    console.log(response.data);
    $scope.tests = response.data;
  }).catch(function(response){
    console.log(response.data);
  });
  $scope.propertyName = 'name';
  $scope.reverse = false;
  $scope.sortBy = function(propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
  };
});

listApp.controller('nodeController', function($scope, $http, $route, $routeParams, $window){
  // create a message to display in our view
  console.log('nodeController');
  var nodeId = $routeParams.nodeId;
  $http({
    method: "GET",
    url: "http://localhost:9000/api/nodes/" + nodeId 
  }).then(function(response) {
    console.log(response.data);
    $scope.node = response.data;
  }).catch(function(response){
    console.log("error with http get");
    console.log(response.data);
    $window.location.href = '/nodes';
  });
});

listApp.controller('nodesController', function($scope, $http, $window){
  $scope.load = function(node){
    $window.location.href = '#!/nodes/' + node._id;
  };
  $http({
    method: "GET",
    url: "http://localhost:9000/api/nodes"
  }).then(function(response) {
    console.log(response.data);
    $scope.nodes = response.data;
  }).catch(function(response){
    console.log(response.data);
  });
  $scope.propertyName = 'name';
  $scope.reverse = false;
  $scope.sortBy = function(propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
  };
});

listApp.controller('exploitController', function($scope, $http, $route, $routeParams, $window){
  // create a message to display in our view
  console.log('exploitController');
  var exploitId = $routeParams.exploitId;
  $http({
    method: "GET",
    url: "http://localhost:9000/api/exploits/" + exploitId 
  }).then(function(response) {
    console.log(response.data);
    $scope.exploit = response.data;
  }).catch(function(response){
    console.log("error with http get");
    console.log(response.data);
    $window.location.href = '/exploits';
  });
});

listApp.controller('exploitsController', function($scope, $http, $window){
  $scope.message = 'This is an exploits message';
  $scope.load = function(exploit){
    $window.location.href = '#!/exploits/' + exploit._id;
  };
  $http({
    method: "GET",
    url: "http://localhost:9000/api/exploits"
  }).then(function(response) {
    console.log(response.data);
    $scope.exploits = response.data;
  }).catch(function(response){
    console.log(response.data);
  });
  $scope.propertyName = 'name';
  $scope.reverse = false;
  $scope.sortBy = function(propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
  };
});

listApp.controller('newTestController', function($scope, $http){
  var getExploits = function(res) {
    $http({
      method: "GET",
      url: "http://localhost:9000/api/exploits"
    }).then(function mySuccess(response) {
        $scope.exploits = response.data;
      }, function myError(response) {
        console.log("error!");
          console.log(response.statusText);
      });
  }
  
  var getNodes = function(res) {
    $http({
      method: "GET",
      url: "http://localhost:9000/api/nodes"
    }).then(function mySuccess(response) {
        $scope.Nodes = response.data;
        var countDef = 0;
        var countOS = 0;
        $scope.filteredDef = [];
        $scope.filteredOS = [];
        for(var i = 0; i < $scope.exploit.works_on.length; i++){
          for(var j = 0; j < $scope.Nodes.length; j++){
            if($scope.exploit.works_on[i].os == $scope.Nodes[j].os && $scope.exploit.works_on[i].patches.indexOf($scope.Nodes[j].patch) != -1){
              $scope.filteredDef[countDef] = $scope.Nodes[j];
              countDef += 1;
              if($scope.filteredOS.indexOf($scope.Nodes[j].os) == -1){
                $scope.filteredOS[countOS] = $scope.Nodes[j].os;
                countOS += 1;
              }
            }
          }
        }
      }, function myError(response) {
          console.log("error!");
          console.log(response.statusText);
      });
  }
  var getPatches = function(){
    console.log("in getPatches()");
    $scope.filteredPatch = []
    var count = 0;
    for(var i = 0; i < $scope.filteredDef.length; i++){
      if($scope.defender_os == $scope.filteredDef[i].os){
        $scope.filteredPatch[count] = $scope.filteredDef[i];
        count += 1;
      }
    }
  }
  $scope.exploitChange = function(){
    console.log($scope.exploit.works_on[0].os);
    getNodes();
  }

  $scope.OSChange = function(){
    console.log($scope.exploit.works_on[0].os);
    console.log($scope.defender_os);
    getPatches();
  }
  
  $scope.submitChange = function(res){
    console.log("in submitChange()");
    var x = document.getElementById("nameError");
    x.style.display = 'none';
    var y = document.getElementById("submitSuccess");
    y.style.display = 'none';
    $http({
      method: "GET",
      url: "http://localhost:9000/api/tests"
    }).then(function(response) {
      console.log(response.data);
      var temp = response.data;
      var check = true;
      if($scope.name == ""){
        console.log("empty test name");
        check = false;
        x.style.display = 'block';
      }
      for(var i = 0; i < temp.length; i++){
        if(temp[i].name == $scope.name){
          console.log("test name already in use");
          check = false;
          x.style.display = 'block';
        }
      }
      console.log(check);
      if(check == true){
        var tname = $scope.name;
        var exp = $scope.exploit._id;
        var att = $scope.attacker_ip.ip;
        var def = $scope.finalDef.ip;
        var data = {
          "TEST": "worked"
        }
        // var data = {
        //   "running": false,
        //   "name": tname,
        //   "exploit_id": exp,
        //   "attacker_ip": att,
        //   "defender_ip": def,
        //   "updates": {}
        // }
        var postData = JSON.stringify(data);
        console.log(postData);
        var req = {
          method: 'POST',
          url: 'http://keblack:aed821ae5ff2d35cc23a55f2f589db86@localhost:8081/job/ParameterPipelineTest/buildWithParameters?token=VkYZz0IYCqObk7ToQjro',
          data: postData
        }
        $http(req).then(function (response){
          console.log("in post success!")
          y.style.display = 'block';
        }).catch(function(response){
          console.log("error wtf!");
          console.log(response);
        });
      }
    }).catch(function(response){
      console.log(response.data);
      check = false;
    })
  }
  
  window.onload = getExploits();
});